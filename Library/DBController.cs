﻿using Microsoft.Data.Sqlite;

namespace VidyakinLibrary
{
    public class DBController
    {
        private SqliteConnection connection = new SqliteConnection("Data Source=userdata.db");
        public DBController()
        {
            connection.Open();
            SqliteCommand command = new SqliteCommand($@"CREATE TABLE IF NOT EXISTS 'Users' (

                'Name'  TEXT NOT NULL,
                'Surname'   TEXT NOT NULL,
                'Passport'  TEXT NOT NULL  CHECK(LENGTH(Passport) = 10) UNIQUE,
                'Role'  INTEGER NOT NULL CHECK(Role >= 0 AND Role <= 2),
                'Password'  TEXT NOT NULL,
                PRIMARY KEY('Passport'))", connection);
            command.ExecuteNonQuery();
            command.CommandText = $@"CREATE TABLE IF NOT EXISTS 'Projects' (
                'Id'    INTEGER NOT NULL UNIQUE,
                'Focus' TEXT NOT NULL,
                'Name'  TEXT NOT NULL UNIQUE,
                'Description'   TEXT NOT NULL,
	            'Authorname'    TEXT NOT NULL,
	            'Authorsurname' TEXT NOT NULL,
	            'Authorpassport'    INTEGER NOT NULL,
                'grade1'    BLOB NOT NULL DEFAULT 0,
	            'grade2'    BLOB NOT NULL DEFAULT 0,
	            'grade3'    BLOB NOT NULL DEFAULT 0,
	            'grade4'    BLOB NOT NULL DEFAULT 0,
	            'recomendation' TEXT,
	            PRIMARY KEY('Id' AUTOINCREMENT)
                )";
            command.ExecuteNonQuery();
        }
        public string insertUser(IUser user, int role, string password)
        {
            try
            {
            if (!long.TryParse(user.getCredits()[2], out long result))
            {
                throw new Exception("Паспорт должен быть числом от 1000000000 до 9999999999");
            }
                SqliteCommand command = new SqliteCommand($@"INSERT INTO 'main'.'Users'
                    ('Name', 'Surname', 'Passport', 'Role', 'Password')
                    VALUES('{user.getCredits()[0]}', '{user.getCredits()[1]}', {user.getCredits()[2]}, {role}, '{password}'); ", connection);
                command.ExecuteNonQuery();
                return "Successfull registration";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public string checkUser(string passport, string password)
        {
            try
            {
                SqliteCommand command = new SqliteCommand($@"SELECT count(*)
                    FROM 'main'.'Users'
                    WHERE Passport={passport} AND Password={password};", connection);
                if (Convert.ToInt32(command.ExecuteScalar()) == 1)
                {
                    return "Valid user data";
                }
                else
                {
                    command = new SqliteCommand($@"SELECT count(*)
                        FROM 'main'.'Users'
                        WHERE Passport={passport}", connection);
                    if (Convert.ToInt32(command.ExecuteScalar()) == 1)
                    {
                        return "Invalid password";
                    }
                    else
                    {
                        return "Invalid user";
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public string addProject(string focus, string name, string description, IUser user)
        {
            try
            {
                SqliteCommand command = new SqliteCommand($@"INSERT INTO 'main'.'Projects'
                    ('Focus', 'Name', 'Description', 'Authorname', 'Authorsurname', 'Authorpassport')
                    VALUES('{focus}', '{name}', '{description}', '{user.getCredits()[0]}', '{user.getCredits()[1]}', {user.getCredits()[2]});", connection);
                command.ExecuteNonQuery();
                return "Successfull project registration";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public string editProject(string name, string recomendation)
        {
            try
            {
                SqliteCommand command = new SqliteCommand($@"UPDATE 'main'.'Projects'
                    SET recomendation = '{recomendation}'
                    WHERE Name = '{name}';", connection);
                if (command.ExecuteNonQuery() == 1)
                    return "Successfull evaluation";
                else
                    return "Unsuccessfull evaluation";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public string editProject(string name, bool grade1, bool grade2, bool grade3, bool grade4, string recomendation)
        {
            try
            {
                SqliteCommand command = new SqliteCommand($@"UPDATE 'main'.'Projects'
                    SET grade1 = {grade1},
                    grade2 = {grade2},
                    grade3 = {grade3},
                    grade4 = {grade4},
                    recomendation = '{recomendation}'
                    WHERE Name = '{name}';", connection);
                if (command.ExecuteNonQuery() == 1)
                    return "Successfull evaluation";
                else
                    return "Unsuccessfull evaluation";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public List<Verdict> getProjects()
        {
            List<Verdict> templist = new List<Verdict>();
            SqliteCommand command = new SqliteCommand("SELECT * FROM 'main'.'Projects'", connection);
            SqliteDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    string focus = reader.GetValue(1).ToString();
                    string name = reader.GetValue(2).ToString();
                    string description = reader.GetValue(3).ToString();
                    string authorname = reader.GetValue(4).ToString();
                    string authorsurname = reader.GetValue(5).ToString();
                    string authorpassport = reader.GetValue(6).ToString();
                    bool grade1 = Convert.ToBoolean(reader.GetValue(7));
                    bool grade2 = Convert.ToBoolean(reader.GetValue(8));
                    bool grade3 = Convert.ToBoolean(reader.GetValue(9));
                    bool grade4 = Convert.ToBoolean(reader.GetValue(10));
                    string recomendation = reader.GetValue(11).ToString();
                    templist.Add(new Verdict(grade1, grade2, grade3, grade4, recomendation, new Request(focus, name, description, authorname, authorsurname,
                        authorpassport)));

                }
            }
            return templist;
        }
    }
}
