﻿namespace VidyakinLibrary
{
    public class Verdict
    {
        private bool grade1 = false;
        private bool grade2 = false;
        private bool grade3 = false;
        private bool grade4 = false;
        private string recomendation = "";
        private Request request;

        public Verdict(Request request)
        {
            this.request = request;
        }

        public Verdict(bool grade1, bool grade2, bool grade3, bool grade4, string recomendation, Request request)
        {
            this.grade1 = grade1;
            this.grade2 = grade2;
            this.grade3 = grade3;
            this.grade4 = grade4;
            this.recomendation = recomendation;
            this.request = request;
        }
        
        public List<bool> getGrades()
        {
            return new List<bool> { grade1, grade2, grade3, grade4};
        }

        public string getRecomendation()
        {
            return recomendation;
        }

        public List<string> getRequestData()
        {
            List<string> data = new List<string>();
            foreach(string str in request.getProjectInfo())
            {
                data.Add(str);
            }
            foreach (string str in request.getCredits())
            {
                data.Add(str);
            }
            return data;
        }

    }
}
