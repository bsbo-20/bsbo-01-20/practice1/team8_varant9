﻿namespace VidyakinLibrary
{
    public class Request
    {
        private string focus;
        private string name;
        private string description;
        private string authorName;
        private string authorSurname;
        private string auhtorPassport;

        public Request(string focus, string name, string description, string authorName, string authorSurname, string auhtorPassport)
        {
            this.focus = focus;
            this.name = name;
            this.description = description;
            this.authorName = authorName;
            this.authorSurname = authorSurname;
            this.auhtorPassport = auhtorPassport;
        }

        public List<string> getProjectInfo()
        {
            return new List<string> { focus, name, description};
        }
        public List<string> getCredits()
        {
            return new List<string> { authorName, authorSurname, auhtorPassport};
        }
    }
}
