﻿namespace VidyakinLibrary
{
    public class AuthHandler
    {
        public int role;
        public IUser user;
        public string createNewUser(DBController controller, string name, string surname, string passport, int role, string password)
        {
            string result = "";
            switch (role)
            {
                case 0:
                    user = new Candidate(name, surname, passport);
                    break;
                case 1:
                    user = new Expert(name, surname, passport);
                    break;
                case 2:
                    user = new FundHolder(name, surname, passport);
                    break;
                default:
                    break;
            }
            this.role = role;
            result = controller.insertUser(user, role, password);
            return result;
        }

        public string checkUserData(DBController controller, string name, string surname, string passport, int role, string password)
        {
            string result = controller.checkUser(passport, password);
            if (result == "Valid user data")
            {
                switch (role)
                {
                    case 0:
                        user = new Candidate(name, surname, passport);
                        break;
                    case 1:
                        user = new Expert(name, surname, passport);
                        break;
                    case 2:
                        user = new FundHolder(name, surname, passport);
                        break;
                    default:
                        break;
                }
                this.role = role;
            }
            return result;
        }

    }
}
