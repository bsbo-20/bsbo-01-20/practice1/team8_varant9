﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VidyakinLibrary
{
    public interface IUser
    {
        public abstract List<List<string>> getAllRequests(ProjectsHandler projectsHandler);
        public List<string> getCredits();
        public List<string> adaptVerdict(Verdict verdict);
    }
}
